FROM python:3.8


RUN pip install --no-cache-dir lxml requests pymongo
ADD counter.py /

CMD [ "python", "./counter.py" ]
