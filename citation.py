from http.server import BaseHTTPRequestHandler
import socketserver
import json
import requests
from lxml import html

PORT = 8000
researcher = ['GIejbKQAAAAJ','xzSFc-AAAAAJ',"ey6GjU4AAAAJ","W4PpmCkAAAAJ","nACJGbQAAAAJ","5-VTFbgAAAAJ","xo03YLEAAAAJ","OC1_7EAAA$

urlbase='https://scholar.google.fr/citations?user='

headers = {'User-Agent': 'Mozilla/7.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0'}

citations =[]

def getcitations (name):
    if 'Wac' in name : user = researcher[0]
    if 'Assassi' in name : user = researcher[1]
    if 'Ciman' in name : user = researcher[2]
    if 'Gustarini' in name : user = researcher[3]
    if 'DeMasi' in name : user = researcher[4]
    if 'Fanourakis' in name : user = researcher[5]
    if 'Tsiourti' in name : user = researcher[6]
    if 'Montanini' in name : user = researcher[7]

    page = requests.get('https://scholar.google.fr/citations?user=' + user, headers=headers)
    tree = html.fromstring(page.content)
    numbers = tree.xpath('//td[@class="gsc_rsb_std"]/text()')



    output = json.dumps({"likes":int(numbers[0])},separators=(',', ':'))
    return output

class JSONRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # send response code:
        self.send_response(200)
        # send headers:
        self.send_header("Content-type", "application/json")
        try:
            output = getcitations(self.path)
        finally:
            self.wfile.write(output.encode())


httpd = socketserver.TCPServer(("", PORT), JSONRequestHandler)

print("serving at port", PORT)
httpd.serve_forever()
