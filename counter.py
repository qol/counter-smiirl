from http.server import BaseHTTPRequestHandler
import socketserver
import json
import requests
from lxml import html
import pymongo
PORT = 8000
researcher = ['GIejbKQAAAAJ']
urlbase='https://scholar.google.fr/citations?user='
headers = {'User-Agent': 'Mozilla/7.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0'}
citations =[]
    
def getcitations (name):
    if 'Wac' in name :
        user = researcher[0]
        page = requests.get('https://scholar.google.fr/citations?user=' + user, headers=headers)
        tree = html.fromstring(page.content)
        numbers = tree.xpath('//td[@class="gsc_rsb_std"]/text()')
        numbers= int(numbers[0])    
        output = json.dumps({"likes":numbers},separators=(',', ':'))
        return output

    elif 'qol' in name :
        client = pymongo.MongoClient('172.26.0.2', 27017)
        db = client.dev
        numbers=db.StudyUser.count_documents({'flowState':"ongoing", "_p_study" : "Study$xScaDGjn5L"})
        client.close()
        output = json.dumps({"likes":numbers},separators=(',', ':'))
        return output

    elif 'wacSunday' in name:
        from datetime import datetime, date
        today = datetime.today()
        year = int(today.strftime('%Y'))
        month = int(today.strftime('%m'))
        day = int(today.strftime('%d'))
        d1 = date(year, month, day)
        d2 = date(2044, 9, 1)
        count = 0
        for d_ord in range(d1.toordinal(), d2.toordinal()):
            d = date.fromordinal(d_ord)
            if (d.weekday() == 6):
                count += 1
        output = json.dumps({"likes":count},separators=(',', ':'))
        return output

    elif 'christmas' in name:
        from datetime import datetime, date
        today = datetime.today()
        year = int(today.strftime('%Y'))
        month = int(today.strftime('%m'))
        day = int(today.strftime('%d'))
        d1 = date(year, month, day)
        d2 = date(year, 12, 25)
        count = 0
        for d_ord in range(d1.toordinal(), d2.toordinal()):
            d = date.fromordinal(d_ord)
            count += 1
        output = json.dumps({"likes":count},separators=(',', ':'))
        return output

    elif 'dateCountdown' in name:
        from datetime import datetime, date
        today = datetime.today()
        year = int(today.strftime('%Y'))
        month = int(today.strftime('%m'))
        day = int(today.strftime('%d'))
        d1 = date(year, month, day)
        d2 = date(year, 10, 18)
        count = 0
        for d_ord in range(d1.toordinal(), d2.toordinal()):
            d = date.fromordinal(d_ord)
            count += 1
        output = json.dumps({"likes":count},separators=(',', ':'))
        return output

    else:return "0"

class JSONRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # send response code:
        self.send_response(200)
        # send headers:
        self.send_header("Content-type", "application/json")
        try:
            output = getcitations(self.path)
        finally:
            self.wfile.write(output.encode())

def main():
    httpd = socketserver.TCPServer(("", PORT), JSONRequestHandler)
    print("serving at port", PORT)
    httpd.serve_forever()
    
main()

